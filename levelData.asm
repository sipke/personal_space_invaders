shopsX:
    .byte 10, 20
shopsY:
    .byte 18,18
shopsColor:
    .byte GREEN, PURPLE
shopNr:
    .byte 2,3    
levelIndex:
    .byte 0,3    
aliensXRow0:
    .byte 30,60,90,0    
aliensXRow1:
    .byte 30,60,90,0    
aliensXRow2:
    .byte 0      
laserHitShopCharBuffer:
    .byte 0,0
timeBeforeDrop:
    .byte 50
//lower means less chance    
chanceOfMovement:
    .byte 10        
moveAmount:    
    .byte 4
nrFramesMoving:
    .byte 4    
