gameLoop:
/*
-laser
    state flying
        clearOldY
        yLarge =-2
        check collide aliens on Y and Y+1
            collide: 
                bullit state collide alien
                alien state dying
                lives --  
                lives =0? 
                    endstate
        check collide shop on Y and Y+1
            collide: 
                bullit state collide shop
                subtract score
    state hit alien: 
        countdown --
        update char
        countdown 0? 
            bullit state none
    state hit shop:
        countdown --
        update char
        countdown 0? 
            bullit state none
-player 
    handle movement    
    fire?
        bullit state none?
            bullit state flying
            draw bullit
not all lowest aliens on shop level?
    countdown until dropdown?
        for each alien:
            aliens down

    */

playerHandler:
    lda playerY
    sta $d00f
    
checkRight:    
    lda $dc00
    cmp #JOY2_RIGHT
    bne checkLeft
	lda playerSpeed 
	cmp #MAX_PLAYER_SPEED
	// already at max speed
    beq !+
	inc playerSpeed
!:  jmp joyDone
checkLeft:
    cmp #JOY2_LEFT
    bne joyDone
// go left	
	lda playerSpeed 
	cmp #MIN_PLAYER_SPEED
	// already at min speed
	beq joyDone
	dec playerSpeed
joyDone:

updatePlayerPos:
{
	lda playerSpeed
	bpl adding
	eor #$FF	
	clc	
	adc #$01	
	sta sm2 +1	
	lda playerX+1
	sec
sm2:	
	sbc #$00
	bcs !+
	dec playerX
!:  sta playerX+1
	jmp posupdate
adding:
	clc
	adc playerX+1
	bcc !+
	inc playerX
!:	sta playerX+1	
posupdate:	
	lda playerX	
	lsr	
	lda playerX+1
	ror 	
	sta $d00e	
// posX = 0 - 640
//MSB if POSXHI = 2
	lda $d010
	and #%01111111
	ldx playerX
	cpx #2
	bne !+
	ora #%10000000
!:	sta $d010

//bounce if posx > 620 or < 20

	ldx playerX
	cpx #2
	bne smallerThan512  //smaller than 512

	lda playerX+1
	cmp #(630-512)
	bcc updateDone
    lda #(630-512)
    sta playerX+1
	jmp invertSpeed
smallerThan512:	
	cpx #1	
	beq	updateDone
//MSB playerX=0    
	lda playerX+1
	cmp #30
	bcs !+
    lda #30
    sta playerX+1
    jmp invertSpeed
!:    
	jmp updateDone
invertSpeed:	
	lda playerSpeed
    bmi !+
    lsr
	eor #$ff
	clc
	adc #$01
    jmp storeSpeed
!:	eor #$ff
	clc
	adc #$01
    lsr
storeSpeed:    
    sta playerSpeed
}
updateDone:		

checkLaser:
    lda $dc00
    cmp #JOY2_FIRE
    bne checkLaserEnd

//FIRE LASER
    lda laserState
    beq fireLaser
    jmp checkLaserEnd
fireLaser:  
//convert player pos to char pos    
//= (x-24)/8 = x/8 -3
// x = 10 bits, so lsr/ror twice then lsr 3x then subtract 1 (about)    
    lda playerX+1
    sta laserX
    lda playerX
    lsr 
    ror laserX
    ldx laserX  //save value for sub char positioning later on
    lsr
    ror laserX
    lsr laserX
    lsr laserX
    lda laserX
    sec
    sbc #1
    sta laserX
    sta laserXAdd + 1
    lda #<LASER_Y_START*40+CHAR_MEM
    clc    
laserXAdd:
    adc #0
    sta LASER_POS_ZP
    lda #LASER_Y_START
    sta laserY
    lda #>LASER_Y_START*40+CHAR_MEM
    adc #0
    sta LASER_POS_ZP + 1
    lda #LASER_STATE_FLYING
    sta laserState
    txa //retreive val from x and tweak
    sec
    sbc #2
    and #$07
    ora #LASER_BASE_CHAR    //sub char positioning by using right char
    sta laserChar

checkLaserEnd:

{
    ldx #3
!:  lda d01f,x
    beq nohit
    eor #$ff
    sta sm1+1
    lda d015,x
sm1:    
    and #0
    sta d015,x
    sta CHAR_MEM+24*40,x
nohit:    
    dex
    bpl !-
}
handleLaser:
    lda laserState
    cmp #LASER_STATE_FLYING
    beq laserDraw
    cmp #LASER_STATE_HIT_SHOP
    beq laserHitShopAnimate
    jmp laserDone
laserDraw:
//clear    
    lda #32
    ldy #0
    sta (LASER_POS_ZP),Y
    ldy #40
    sta (LASER_POS_ZP),Y
//update pos
    lda LASER_POS_ZP
    sec
    sbc #40
    sta LASER_POS_ZP
    lda LASER_POS_ZP +1
    sbc #0
    sta LASER_POS_ZP +1
    cmp #>CHAR_MEM
    bcs checkShopHitAndDrawLaser
//out of view:    
    lda #LASER_STATE_NONE
    sta laserState
    jmp laserDone
checkShopHitAndDrawLaser:
    //if new pos is not space, then shop is hit    
    ldy #0//laserX
    lda (LASER_POS_ZP),Y
    cmp #$20
    bne shopHit
//draw laser chars
    lda laserChar
    ldy #0
    sta (LASER_POS_ZP),Y
    ldy #40
    sta (LASER_POS_ZP),Y

    jmp laserDone
laserHitShopAnimate:
    ldy #0
    lda (LASER_POS_ZP),y
    sec
    sbc #1
    sta (LASER_POS_ZP),y
    ldy #40
    sta (LASER_POS_ZP),y
    cmp #LASER_HIT_SHOP_END_CHAR
    bne !+    
    lda #LASER_STATE_NONE
    sta laserState
//restore original chars
    ldy #0    
    lda laserHitShopCharBuffer
    sta (LASER_POS_ZP),Y
    ldy #40
    lda laserHitShopCharBuffer+1
    sta (LASER_POS_ZP),Y
!:  jmp laserDone
shopHit:
//save original chars
    ldy #0
    lda (LASER_POS_ZP),Y
    sta laserHitShopCharBuffer
    ldy #40
    lda (LASER_POS_ZP),Y
    sta laserHitShopCharBuffer+1

    lda #LASER_HIT_SHOP_START_CHAR
    sta (LASER_POS_ZP),Y
    ldy #40
    sta (LASER_POS_ZP),Y
    lda #LASER_STATE_HIT_SHOP
    sta laserState

laserDone:

