

// PETSCII memory layout (example for a 40x25 screen)
// byte  0         = border color
// byte  1         = background color
// bytes 2-1001    = screencodes
// bytes 1002-2001 = color
screen_001:
    .encoding "screencode_mixed"
         //1234567890123456789012345678901234567890 
    .text "                                        " 
    .text "       THE ALIENS ARE COMING!           "
    .text "                                        " 
    .text "             TO SHOP!                   "
    .text "                                        "
    .text "     They bring much needed cash for    "
    .text "                                        "
    .text "         our ailing economy.            "
    .text "                                        "
    .text "It's your job to make sure they practice"
    .text "                                        "
    .text " social distancing by firing a warning  "
    .text "                                        "
    .text "  blast between aliens coming too close "
    .text "                                        "
    .text "          to each other.                "
    .text "                                        "
    .text "  Be careful not to hit the shops and   "
    .text "                                        "
    .text "          NEVER SHOOT AN ALIEN!         "
    .text "                                        "
    .text "         We don't want a repeat         "
    .text "                                        "
    .text "       of the 1978 Taito incident!      "
    .text "                                        "

    .fill 1000,GREEN