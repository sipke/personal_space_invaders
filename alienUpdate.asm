{
inc $d020    
    ldx alienAnimationCounter
    inx
    stx alienAnimationCounter
    txa
    and #$07
    bne !+

    ldy #27
!:  lda pointer,y
    eor #$01
    sta pointer,y
    dey
    bpl !-

    ldx #0
//    stx currentAlien
alienUpdateLoop:    
//    ldx currentAlien
    lda alienState,x
    cmp #ALIEN_STATE_NEUTRAL
    beq !+
    jmp checkStateMoving
!:    
    jsr getRandom
    lda rand
    cmp chanceOfMovement    //rand smaller? then set to moving
    bcc !+
    jmp next
!:  lda seedhi
    bpl setMoveRight
    lda moveAmount
    eor #$ff
    clc
    adc #1
    jmp setMove
setMoveRight:
    lda moveAmount
setMove:
    sta vx,x
    lda nrFramesMoving
    sta moveCounter,x
    lda #ALIEN_STATE_MOVING
    sta alienState,x
    jmp next
checkStateMoving:
    cmp #ALIEN_STATE_MOVING
beq !+
    jmp checkStateDying
!:    
    lda vx,x
    bpl positiveSpeed
    lda xPos,x
    sec
    sbc #1
    bcs !+
    lda #160
!:  sta xPos,x
    jmp lpEnd
positiveSpeed:
    lda xPos,x
    clc
    adc #$01
    cmp #160
    bne !+
    lda #0
!:  sta xPos,x
lpEnd:    
    dec moveCounter,x
    bne next
    lda #ALIEN_STATE_NEUTRAL
    sta alienState,x
checkStateDying:
    cmp #ALIEN_STATE_DYING
beq !+
    jmp checkStateShopping
!:

//TODO no shopping?
checkStateShopping:
//     cmp #ALIEN_STATE_SHOPPING
// beq !+
//     jmp next
// !:

next:
    inx
    cpx #28
    beq end
    jmp alienUpdateLoop
end:
//moveDown?
    dec moveDownCounter
    bne noMoveDown
    ldx #3
!:  lda yPos,x
    clc
    adc #8
    sta yPos,x
.break
    inc yPosLarge,x
    dex
    bpl !-
noMoveDown:    
dec $d020    
}
/*
-aliens
    dying?
        animate dying 
        counter --
        counter = 0 : remove
    Y = shops Y?
        check hit shop?
            state = scoring

        x+=max speed, 
            x<0 | x> 320?
            reverse speed
        moving > 0?
            x+=speed
            moving --
        moving = 0 ?
            getRandom < boundary?
                moving = amount
                speed = levelspeed + or minus
    state scoring:
        update
        counter --
        counter 0
            remove alien                
*/

