//main.asm
.import source "macros.asm"
.import source "constants.asm"
.pc = $0801 "Basic upstart"
:BasicUpstart($080d)		
.pc = $080d "setup" 

//setup
    ldx #0
    stx $d020
    stx $d021

    lda #D016_BASE_VALUE
    sta $d016

    jsr initIrq


    buildStartScreen(titleScreenData)

    jsr insertCustomChars
    setCharsAt8800()
    jsr copySprites
    cli
*=* "main title loop"    
mainTitleLoop:    
    jsr getRandom
    lda $dc00
    cmp #JOY2_FIRE
    beq doFire
    cmp #JOY2_RIGHT
    bne mainTitleLoop
    jsr initScrollLeft
    jmp mainTitleLoop
*=* "game setup"        
doFire: 
    clearCharsTopDown()
    ldx #0
!:    
.for(var i=0;i<1000;i+=250){
    sta $d800+i*250,x
}
    inx
    cpx #250
    bne !-
    jsr initGameSprites
    setIntTo(ingameIrq, 0)
    jsr buildGameScreen
    lda #GAME_STATE_RUNNING
    sta gameState

*=* "non irq loop"
.import source "nonIRQLoop.asm"    

*=* "insert custom chars code"
insertCustomChars: 
    insertCustomCharsToCharsetM(256,customChars, CHARSET_MEM, 0)
    rts
shopPETSCII:
    .byte 92,95,95,91
    .byte 147,136,143,144
    .byte 95,94,93,95
gameState:
    .byte 0
level:
    .byte 0
score:
    .fill 4,0  
shopTopY:
    .byte 0      
shopBottomY:
    .byte 0      
*=* "title screen"    
.import source "titleScreen.asm"
*=* "level data"    
.import source "levelData.asm"
*=* "custom chars"    
customChars:
.import binary "./resources/pet_low_expanded_reverts - Chars.bin"
*=* "mulitplex code"    
.import source "multiplexSpriteIrq.asm"
*=* "interrupt code"    
.import source "interrupt.asm"
*=* "instruction screen"    
instructionScreenSource:
    .import source "resources\instrcutionScreen.asm"
*=* "assorted code"        
    .import source "routines.asm"
*=* "startY tables"        
startYCharHi:
    .fill 25, >CHAR_MEM + 40*i
startYCharLo:
    .fill 25, <CHAR_MEM + 40*i
initScrollLeft:
    setIntTo(scrollLeftint, 30)
    rts
*=* "large scroll speedcode"    
largeScroll:

    lda #57
!:  cmp $d012
    bne !-

    ldy columnsDone
    scrollLeftToRight(instructionScreenSource, instructionScreenSource+1000, true)

    lda #30
!:  cmp $d012
    bne !-

    lda #D016_BASE_VALUE + 4
    sta $d016   
    scrollLeftToRight(instructionScreenSource, instructionScreenSource+1000, false)
    iny
    sty columnsDone

    endIrq()

    