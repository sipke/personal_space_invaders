#importonce

.const SCROLL_SPEED = 4
.const D016_BASE_VALUE = $c8

//----------------------- JOYSTICK
.const JOY1_FIRE = 239
.const JOY1_LEFT = 251
.const JOY1_RIGHT = 247
.const JOY2_FIRE = 111
.const JOY2_LEFT = 123
.const JOY2_RIGHT = 119

//-------------------------- COLOR PETSCII CODES
.const COL_CODE_WHITE = 5
.const COL_CODE_RED = 28
.const COL_CODE_GREEN = 30
.const COL_CODE_BLUE = 31
.const COL_CODE_ORANGE = 129
.const COL_CODE_BLACK = 144
.const COL_CODE_BROWN = 149
.const COL_CODE_LIGHT_RED = 150
.const COL_CODE_LIGHT_GREY = 151
.const COL_CODE_GREY = 152
.const COL_CODE_LIGHT_GREEN = 153
.const COL_CODE_LIGHT_BLUE = 154
.const COL_CODE_DARK_GREY = 155
.const COL_CODE_PURPLE = 156
.const COL_CODE_YELLOW = 158
.const COL_CODE_CYAN = 159
//------------------------------ IN GAME
.const LASER_DURATION = 7
.const LASER_BASE_CHAR = 192
.const LASER_COLOR = YELLOW

.const CHAR_MEM = $8000 //-8400
.const SPRITE_POINTERS = CHAR_MEM + $3f8
.const CHARSET_MEM = $8800 //$9000
//9000-9fff unsuable but don't need that much video mem anyway
.const SPRITE_MEM_START= $a000

.const ALIEN_STATE_DYING =1
.const ALIEN_STATE_NEUTRAL =0
.const ALIEN_STATE_MOVING = 2
.const ALIEN_STATE_SHOPPING = 3
.const ALIEN_STATE_DEAD = 4

.const MAX_PLAYER_SPEED = 6
.const MIN_PLAYER_SPEED = -6
.const LASER_STATE_NONE = 0
.const LASER_STATE_FLYING = 1
.const LASER_STATE_HIT_SHOP = 2
.const LASER_STATE_HIT_ALIEN = 3
.const LASER_POS_ZP = $f9 //fa
.const LASER_Y_START = 23
.const LASER_HIT_SHOP_START_CHAR = 42
.const LASER_HIT_SHOP_END_CHAR = 33

.const GAME_STATE_INTRO = 0
.const GAME_STATE_ENDING = 1
.const GAME_STATE_STARTING = 2
.const GAME_STATE_RUNNING = 3
.const GAME_STATE_HISCORE = 4