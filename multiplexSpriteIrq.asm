// assume max 8 sprites per line (DUH!)
// max 4 lines of aliens
// move downwards in steps of 8 (or mulitple of) so can circumvent bad lines
   

initializeSprites:

ingameIrq:
    startIrq()

.for(var line=0; line<4; line++) {

    //just before repositioning save collision state
    .if(line!=0){
        lda $d01f
        sta d01f + line -1
    }
//a few lines before actual Y pos, set sprites
    ldy yPos + line
    tya
    sbc #6
!:  cmp $d012
    bne !-

    lda #0
    sta $d010   //initialize MSB
    
    .for(var s=0; s<7; s++) {
        lda xPos + line*7 + s   //xpos x 2, overflow -> d010
        asl
        ror $d010
        sta $d000 + s*2
        lda pointer + s + line*7
        sta SPRITE_POINTERS + s
        lda alienColor + s
        sta $d027 + s
        sty $d001 + s*2
    }
    clc
    ror $d010
//wait for actual line (TODO: maybe before that?)    
    tya
!:  cmp $d012
    bne !-
//turn sprites on
    lda d015 + line
    sta $d015

}
//final save collision state, after complete sprite is shown (TODO: is this necessary?)
    tya 
    adc #21
!:  cmp $d012
    bne !-

    lda $d01f
    sta d01f + 3

.import source "alienUpdate.asm"
.import source "gameloop.asm"

    endIrq()


alienState:
    .fill 28,0
moveCounter:
    .fill 28,0    
alienAnimationCounter:
    .byte 0
xPos:
    .for(var i=0; i<28; i++){
        .byte random()*160
    }
vx:
    .fill 7, floor(random()*4)-2
    .fill 7, floor(random()*4)-2
    .fill 7, floor(random()*4)-2
    .fill 7, floor(random()*4)-2
yPos:
    .byte 60,84,108,132
//for easy comparison with characters Y    
yPosLarge:
    .byte floor(60-24)/8,(84-24)/8,(108-24)/8,(132-24)/8

pointer:
    .fill 7, random()*6 + floor((SPRITE_MEM_START & $3fff)/64)
    .fill 7, random()*6 + floor((SPRITE_MEM_START & $3fff)/64)
    .fill 7, random()*6 + floor((SPRITE_MEM_START & $3fff)/64)
    .fill 7, random()*6 + floor((SPRITE_MEM_START & $3fff)/64)

d015:
    .byte %11111111
    .byte %11111111
    .byte %11111111
    .byte %11111111

d01f:
    .byte 0,0,0,0

alienColor:
    .fill 7, 2+random()*14
    .fill 7, 2+random()*14
    .fill 7, 2+random()*14
    .fill 7, 2+random()*14

playerX: //16 bit
    .byte 50,0
playerY:
    .byte 230    
playerSpeed:
    .byte 0
laserCoolDown:
    .byte 0
laserX:
    .byte 0
laserY:
    .byte 0
laserChar:
    .byte 0    
startPosLo:
    .fill 8, <(CHAR_MEM+i*120)
startPosHi:
    .fill 8, >(CHAR_MEM+i*120)
laserState:
    .byte 0    
currentAlien:
    .byte 0
moveDownCounter:
    .byte 0    


spritesOnLoad:
.import binary "resources\sprites - Sprites.bin"
spritesOnLoadEnd:
copySprites:
{
    ldy #ceil((spritesOnLoadEnd-spritesOnLoad)/256)
    ldx #0
loop:    
    lda spritesOnLoad,x
    sta SPRITE_MEM_START,x
    inx
    bne loop
    inc loop+2
    inc loop+5
    dey
    bne loop
    rts
}    

