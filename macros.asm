.var brkFile = createFile("debug.txt") 
.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}
.macro setIntTo(newAddress, line){
	sei
	lda #<newAddress
	sta $fffe
	lda #>newAddress
	sta $ffff

	lda #<line
	sta $d012
	.if(line>255) {
		lda $d011
		ora #$80
		sta $d011
	}else {
		lda $d011
		and #$7f
		sta $d011
	}
	cli

}

.macro startIrq(){
	pha        //store register A in stack
	txa
	pha        //store register X in stack
	tya
	pha        //store register Y in stack

    // lsr $d019
    // bcs !+
    // jmp $ea31
!:	//inc $d020
}
.macro endIrq(){
	//dec $d020
    //acknowledge irq
    lda #$ff
    sta $d019
    // jmp $ea81
	pla
	tay        //restore register Y from stack (remember stack is FIFO: First In First Out)
	pla
	tax        //restore register X from stack
	pla        //restore register A from stack
	rti        //Re
}
.macro buildStartScreen(titleScreenData){
	ldx #0
!:
	.for(var i=0; i<4; i++)    {
		lda titleScreenData+i*250,x
		sta CHAR_MEM+i*250,x
		lda titleScreenData+1000+i*250,x
		sta $d800+i*250,x
	}
    inx
    cpx #250
    bne !-

}



.macro setCharsAt8800(){	
    lda $dd00
    and #%11111100
    ora #%00000001
    sta $dd00

	lda	#$02
	sta	$d018
}
.macro setCharsAtDefault(){	
	lda	#$15
	sta	$d018
}
.macro insertCustomCharsToCharsetM(nrChars, newCharStart, charSetMemStart, fromChar) {
    ldy #ceil(nrChars*8)/256
	ldx #0
loop:	
	lda newCharStart,x
	sta charSetMemStart,x
	inx
	bne loop
	inc loop +2
	inc loop +5
	dey
	bne loop
}

//	small x scroll: just before start screen
// large scroll, in frame before scroll just after screen visible copy all

//y is assumed to be column of the source to scroll into view
.macro scrollLeftToRight(charSource, colorSource, topHalf) {
	.var startY=0
	.var endY=11
	.if (!topHalf) {
		.eval startY = 11
		.eval endY = 25
	}
	.for(var y=startY; y<endY; y++){
		.for(var x=1; x<40; x++) {
			lda $d800 + x + 40*y
			sta $d800 + x + 40*y - 1
			lda CHAR_MEM + x + 40*y
			sta CHAR_MEM + x + 40*y - 1
		}
		lda charSource + 40*y,y
		sta CHAR_MEM + 40*y+39
		lda colorSource + 40*y,y
		sta $d800 + 40*y+39
	}
}
//y is assumed to be column of the source to scroll into view
.macro scrollRightToLeft(charSource, colorSource) {
	.for(var y=0; y<25; y++){
		.for(var x=39; x>0; x--) {
			lda $d800 + x + 40*y +1
			sta $d800 + x + 40*y
			lda CHAR_MEM + x + 40*y +1
			sta CHAR_MEM + x + 40*y
		}
	}	
	.for(var y=0; y<25; y++){
		lda charSource + 40*y,y
		sta CHAR_MEM + 40*y
		lda colorSource + 40*y,y
		sta $d800 + 40*y
	}
}
.macro clearCharsTopDown(){
	lda #<CHAR_MEM
    sta sm1 +1
    lda #>CHAR_MEM
    sta sm1+2
    lda #32
sm1:
    sta CHAR_MEM
    ldx #0
!:  inx
    bne !-    
    inc sm1+1
    bne !+
    inc sm1+2
!:  lda sm1+2  
    cmp #>CHAR_MEM+$03e8
    bne sm1 -2
    lda sm1+1  
    cmp #<CHAR_MEM+$03e8
    bne sm1 -2
    lda #LASER_COLOR

}

