	.struct RGB {r,g,b}

// 00 00 00
// FF FF FF
// 68 37 2B
// 70 A4 B2
// 6F 3D 86
// 58 8D 43
// 35 28 79
// B8 C7 6F
// 6F 4F 25
// 43 39 00
// 9A 67 59
// 44 44 44
// 6C 6C 6C
// 9A D2 84
// 6C 5E B5
// 95 95 95
/*        
			RGB(0,0,0),		// black 0
			RGB(255,255,255),	// white 1
			RGB(104,55,43),		// red 2
			RGB(131,240,220),	// cyan 3
			RGB(111,61,134),	// purple 4
			RGB(89,205,54),		// green 5
			RGB(65,55,205),		// blue 6
			RGB(184,199,111),	// yellow 7
			RGB(209,127,48),	// orange 8
			RGB(67,57,0),		// brown 9	
			RGB(154,103,89),	// light_red 10
			RGB(91,91,91),		// dark_gray 11
			RGB(142,142,142),	// gray 12
			RGB(157,255,157),	// light_green 13
			RGB(117,161,236),	// light_blue 14
			RGB(193,193,193)	// light_gray 15
*/            
	.var s_palette = List().add(
            RGB($00,$00,$00),
            RGB($FF,$FF,$FF),
			RGB($9f,$e4,$44),
			RGB($6a,$bf,$c6),
			RGB($a0,$57,$a3),
			RGB($5c,$ab,$5e),
			RGB($50,$49,$5b),
			RGB($c9,$d4,$87),
			RGB($a1,$68,$3c),
			RGB($6d,$54,$12),
			RGB($cb,$7e,$75),
			RGB(91,91,91),
			RGB(142,142,142),
			RGB ($9a,$e2,$9b),
			RGB ($6a,$bf,$c6),
			RGB ($ad,$ad,$ad)
		)

/*
            RGB($00,$00,$00),
            RGB($FF,$FF,$FF),
            RGB($68,$37,$2B),
            RGB($70,$A4,$B2),
            RGB($6F,$3D,$86),
            RGB($58,$8D,$43),
            RGB($35,$28,$79),
            RGB($B8,$C7,$6F),
            RGB($6F,$4F,$25),
            RGB($43,$39,$00),
            RGB($9A,$67,$59),
            RGB($44,$44,$44),
            RGB($6C,$6C,$6C),
            RGB($9A,$D2,$84),
            RGB($6C,$5E,$B5),
            RGB($95,$95,$95)
		*/
	
	.function colorDistance(c1,c2)
	{
		.var cr = c1.r-c2.r
		.var cg = c1.g-c2.g
		.var cb = c1.b-c2.b
		.return sqrt([cr*cr] + [cg*cg] + [cb*cb])
	}

	.function getClosestColorIndex(rgb)
	{
		.return getClosestColorIndex(
			rgb, s_palette
			)
	}

	.function getClosestColorIndex(rgbI, palette)
	{
        
        .var rgb= RGB(rgbc>>16, (rgbc>>8)&$ff, rgbc & $ff)
		.var distance = colorDistance(rgb, palette.get(0))
		.var closestColorIndex = 0

		.for (var index = 1; index < palette.size(); index++)
		{
			.var d = colorDistance(rgb, palette.get(index))
			.if (d < distance)
			{
				.eval distance = d
				.eval closestColorIndex = index
			}
		}

		.return closestColorIndex
	}