nonIRQLoop:
    lda gameState
    cmp #GAME_STATE_RUNNING
    beq game
    jmp checkGameStateEnding
game:
    .for (var i=3; i>=0;i--){
        lda yPosLarge + i
        cmp shopsY
        bcs !+
        jmp next
!:        
.break
        ldx #8
loop:                
        lda xPos+i*7,x
//far to the right: move left
//far to the left: move right
//otherwise keep moving?        
        cmp #100
        bcs !+
        lda #4
        sta vx+i*7,x
        lda #ALIEN_STATE_MOVING
        sta alienState+i*7,x
        lda #4
        sta moveCounter+i*7,x
        jmp x
!:        
        lda #-4
        sta vx+i*7,x
x:        
        dex
        bpl loop
    next:
    }

    jmp nonIRQLoop

checkGameStateEnding:

    jmp nonIRQLoop