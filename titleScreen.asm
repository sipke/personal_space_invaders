// main screen
// convert png to 4x4
.import source "colorMatch.asm"

titleScreenData:

//using https://codebase64.org/doku.php?id=base:kick_assembler_macros

// should be 80x40 
// 2 lines above for highscore and empty line
// 3 line below for '(c)' and 'instructions >> '
.const RawImage = LoadPicture("./resources/title_2.png")

.var chars =List()
.var colors =List()
.var b=0
.var c=0
.var cellColor=0
.var printed = 0
.var rgbc
.var s=""
.var s2=""
.for (var y=0; y<40; y+=2) {
    .eval s=""
    .for (var x=0; x<80; x+=2) {
        .eval b=0
        .eval c=0
        .eval cellColor =0
        
        .for(var dy=0; dy<2; dy++){
            .eval s=""
            .for(var dx=0; dx<2; dx++){
                .eval rgbc = RawImage.getPixel(x+dx,y+dy)
                .eval c = getClosestColorIndex(rgbc)

                .if(c!=0) {
                    .eval cellColor = c
                }
                .eval b = b + (c==0 ?  0 : pow(2,(mod(x+dx,2))+ 2* mod(y+dy,2)))
            }
        }

        .eval chars.add(b)
        .eval colors.add(cellColor)
    }

}

.assert "should have 20x40 chars", chars.size(), 800
.assert "should have 20x40 colors", colors.size(), 800

.encoding "screencode_mixed"
//     1234567890123456789012345678901234567890 
.text "         Highscore 05000 Goerp          "
.text "                                        "
.for(var i=0;i<chars.size(); i++) {
    .byte chars.get(i)+240
}
//     1234567890123456789012345678901234567890 
.text "                                        "
.text "         (c) 2020  Goerp                "
.text "port II                  instructions >>"

.fill 40, RED
.fill 40, BLACK
.for(var i=0;i<colors.size(); i++) {
    .byte colors.get(i)
}
.fill 40, BLACK
.fill 40, PURPLE
.fill 22, CYAN
.fill 18, GREEN

