//color= A
//X: Row 
//Y: Column
drawShop:
{
    stx shopTopY
    stx shopBottomY
    inc shopBottomY
    inc shopBottomY
    sta col
    lda startYCharHi,x
    sta $fc
    clc
    adc #>($d800-CHAR_MEM)
    sta $fe
    lda startYCharLo,x
    sta $fb
    sta $fd

//    ldy #0
    ldx #0
loop:    
    lda shopPETSCII,x
    sta ($fb),y
    lda col
    sta ($fd),y
    iny
    inx
    cpx #4
    txa
    cpx #4
    beq !+
    cpx #8
    bne noNewLine
!:    
    tya
    clc
    adc #40-4
    tay
noNewLine:
    cpx #12
    bne loop 
    rts
col:
    .byte 0    
}

buildGameScreen:
{
    ldx level
    lda shopNr,x
    clc
    adc levelIndex,x
    sta sm1+1
    lda levelIndex,x
    sta index
loop:    
    ldx index
    lda shopsColor,x
    pha
    ldy shopsX,x
    lda shopsY,x
    tax
    pla
    jsr drawShop
    ldx index
    inx
    stx index
sm1:
    cpx #0
    bne loop
    rts
index:
    .byte 0
}



initGameSprites:
    lda #$ff
    sta $d01c

    lda #>320
    sta playerX
    lda #<320
    sta playerX+1

    //
    lda #WHITE
    sta $d025
    sta $d026
    sta $d027+7
    lda #(SPRITE_MEM_START/64)+9
    sta SPRITE_POINTERS+7

//init states
    lda #LASER_STATE_NONE    
    sta laserState
    rts

getRandom:
  lda seedhi
  lsr
  rol seedlo
  bcc noeor
  eor #$B4
noeor:
  sta seedhi
  eor seedlo
  sta rand
  rts
seedlo:
    .byte random()*254+1
seedhi:
    .byte random()*254+1
rand:
    .byte 0    